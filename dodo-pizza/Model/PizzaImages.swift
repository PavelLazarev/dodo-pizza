//
//  PizzaImages.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 11.07.2023.
//

import Foundation

struct PizzaImages {
    
    enum Images: String {
        case Домашняя = "Домашняя"
        case ЦипленокКарри = "ЦипленокКарри"
        case Маргарита = "Маргарита"
        case ЧетыреСыра = "ЧетыреСыра"
        case Гавайская = "Гавайская"
    }
}


