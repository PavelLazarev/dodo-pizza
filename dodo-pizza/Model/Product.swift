//
//  Product.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 30.06.2023.
//

import Foundation
import UIKit

struct Products: Decodable {
    let products: [Product]
}

struct Product: Decodable {
    var id: Int
    var name: String
    var description: String
    var price: Int
    var image: String?
    
    
}

extension Products {
    func loadImage(_ name: String) -> UIImage? {
        return UIImage(named: name)
    }
}
