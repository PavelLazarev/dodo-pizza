//
//  Stack.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 30.06.2023.
//

import Foundation
import UIKit

enum StackViewStyle {
    case vertical
}

class StackView: UIStackView {
    
    init(type: StackViewStyle) {
        super.init(frame: .zero)
        
        switch type {
            
        case .vertical:
            configureVerticalStackView()
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureVerticalStackView() {
        self.axis = .vertical
        self.spacing = 15
        self.alignment = .leading
        
        self.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 10, leading: 15, bottom: 12, trailing: 0)
        self.isLayoutMarginsRelativeArrangement = true
    }
}
