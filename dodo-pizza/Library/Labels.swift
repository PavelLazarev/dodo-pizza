//
//  Label.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 30.06.2023.

import UIKit

enum LabelStyle {
    case name
    case detail
    case sum
}

final class Labels: UILabel {
    
    init(type: LabelStyle) {
        super.init(frame: .zero)
        
        switch type {
            
        case .name:
            configureNameLabel()
        case .detail:
            configureDetailLabel()
        case .sum:
            configureSumLabel()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   private func configureNameLabel() {
        self.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
   private func configureDetailLabel() {
        self.textColor = .darkGray
        self.numberOfLines = 0
        self.font = UIFont.boldSystemFont(ofSize: 15)
    }
    
    private func configureSumLabel() {
        self.textColor = .black
        self.font = UIFont.boldSystemFont(ofSize: 15)
    }
    
}
