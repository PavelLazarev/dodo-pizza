//
//  Buttons.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 30.06.2023.


import Foundation
import UIKit

enum ButtonStyle {
    case price
}

class Buttons: UIButton {
    
    init(type: ButtonStyle) {
        super.init(frame: .zero)
        
        switch type {
        case .price:
            configurePriceButton()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configurePriceButton() {
        self.backgroundColor = .orange.withAlphaComponent(0.1)
        self.layer.cornerRadius = 20
        self.setTitleColor(.brown, for: .normal)
    }
    
}
