//
//  NetworkService.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 07.07.2023.
//

import Foundation

enum Link: String {
    case linkUrl = "https://apingweb.com/api/rest/8325141636869e856901a7a565b0b3e123/products"
}

enum NetworkError: Error {
    case invalidURL
    case noData
    case decodingError
}

final class NetworkService {
    private let jsonDecoder = JSONDecoder()
    
    func fetchProducts(from url: String?, completion: @escaping (Result<Products, NetworkError>) ->()) {
        guard let url = URL(string: url ?? "") else {
            completion(.failure(.invalidURL))
            return
        }

        URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data else {
                completion(.failure(.noData))
                return
            }
            do {
                let products = try self.jsonDecoder.decode(Products.self, from: data)
                DispatchQueue.main.async {
                    completion(.success(products))
//                    print(products)
                }
            }
            catch {
                completion(.failure(.decodingError))
            }
        }
        .resume()
    }
    
}
// вопросы:
// правильно-ли сделан запро fetchProducts
