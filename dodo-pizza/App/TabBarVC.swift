//
//  TabBarVC.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 04.07.2023.
//

import UIKit

final class TabBarVC: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    private func setup() {
        
        let menuScreenVC = MenuScreenVC()
        let menuNavigationController = UINavigationController(rootViewController: menuScreenVC)
        
        let menuImage = UIImage(systemName: "menucard")
        let menuSelectedImage = UIImage(systemName: "menucard.fill")
        let menuTabItem = UITabBarItem(title: "Меню", image: menuImage, selectedImage: menuSelectedImage)
        
        menuScreenVC.title = menuTabItem.title
        menuNavigationController.tabBarItem = menuTabItem
        
        let shoppingCartVC = ShoppingCartVC()
        let shoppingNavigationController = UINavigationController(rootViewController: shoppingCartVC)
        let shoppingImage = UIImage(systemName: "cart")
        let shoppingSelectedImage = UIImage(systemName: "cart.fill")
        let shoppingTabItem = UITabBarItem(title: "Корзина", image: shoppingImage, selectedImage: shoppingSelectedImage)
        
        shoppingCartVC.title = shoppingTabItem.title
        shoppingNavigationController.tabBarItem = shoppingTabItem
        
        
        
        viewControllers = [menuNavigationController, shoppingNavigationController]
    }
}
