//
//  ShoppingCartVC.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 30.06.2023.
//
//
import UIKit

final class ShoppingCartVC: UIViewController {
    
    private let networkService = NetworkService()
    private let confirmPriceButton = ConfirmButtonFooterView()
    private lazy var priceHeaderView = PriceHeaderView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 60)) //чтобы header view появился
    private var products = [Product]()
    private var totalPrice: Int?

    private lazy var cartTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.backgroundColor = .white

        tableView.register(ProductCartCell.self, forCellReuseIdentifier: ProductCartCell.reuseId)
        tableView.register(SumAndCountCell.self, forCellReuseIdentifier: SumAndCountCell.reuseId)
        
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        setupViews()
        setupConstraints()
        
        fetchProducts()

//
//        confirmPriceButton.onAction = {
//            print(#line)
//        }
        
    }
    
    private func fetchProducts() {
        networkService.fetchProducts(from: Link.linkUrl.rawValue) { [weak self] result in
            switch result {
                
            case .success(let products):
                self?.products.append(contentsOf: products.products)
                self?.totalPrice = self?.calculateTotalPrice()
                self?.priceHeaderView.update(count: self?.products.count ?? 0, price: self?.totalPrice ?? 0)
                self?.confirmPriceButton.update(self?.totalPrice ?? 0)
                DispatchQueue.main.async {
                    self?.cartTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func calculateTotalPrice() -> Int {
        var totalPrice = 0
        for element in products {
            totalPrice += element.price
        }
        return totalPrice
    }
    
}

extension ShoppingCartVC {
    
    private func setupViews() {
        view.addSubview(cartTableView)
        view.addSubview(confirmPriceButton)
    }
    
    private func setupConstraints() {
        cartTableView.snp.makeConstraints { make in
            make.left.right.top.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(confirmPriceButton.snp.top)
        }
        confirmPriceButton.snp.makeConstraints { make in
            make.left.right.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
        
    }
}

extension ShoppingCartVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { // кол-во ячеек в секции
        return products.count * 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return priceHeaderView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        //[(0,0), (0,1), (0,2)]
//        print(indexPath)
        //0 -> 0, 1 -> 0, 2 -> 1, 3 -> 1,
        let index = indexPath.row / 2 // индекс ячейки / 2

        if indexPath.row % 2 == 0 {
            guard let productCell = tableView.dequeueReusableCell(withIdentifier: ProductCartCell.reuseId, for: indexPath) as? ProductCartCell
            else {
                return UITableViewCell()
            }
            let product = products[index]
            productCell.update(product: product)
            
            return productCell
        } else {
            guard let sumAndCountCell = tableView.dequeueReusableCell(withIdentifier: SumAndCountCell.reuseId, for: indexPath) as? SumAndCountCell
            else {
                return UITableViewCell()
            }
            let product = products[index]
            sumAndCountCell.update(product: product)
            
            
            return sumAndCountCell
        }
    }

}
