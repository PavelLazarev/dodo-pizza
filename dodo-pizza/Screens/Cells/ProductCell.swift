//
//  ProductCell.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 30.06.2023.
//

//import Foundation
import UIKit

final class ProductCell: UITableViewCell {
    
    static let reuseId = "ProductCell"
    
    private let nameLabel = Labels(type: .sum)
    private let detailLabel = Labels(type: .detail)
    private let priceButton = Buttons(type: .price)
    private let verticalStackView = StackView(type: .vertical)
    
    let productImageView: UIImageView = { // поместить в отдельный класс?
        var imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        let width = UIScreen.main.bounds.width
        imageView.heightAnchor.constraint(equalToConstant: 0.40 * width).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 0.40 * width).isActive = true
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(product: Product) {
        nameLabel.text = product.name
        detailLabel.text = product.description
        priceButton.setTitle("\(product.price) р", for: .normal)
        
        if product.image == "1" {
            productImageView.image = UIImage(named: "Домашняя")
        } else if product.image == "2" {
            productImageView.image = UIImage(named: "Цыпленок карри")
        } else if product.image == "3" {
            productImageView.image = UIImage(named: "Маргарита")
        } else if product.image == "4" {
            productImageView.image = UIImage(named: "4 сыра")
        } else {
            productImageView.image = UIImage(named: "Гавайская")
        }
    }
}


extension ProductCell {
    
    struct Layout {
        static let offset = 16
    }
    
    func setupViews() {
        contentView.addSubview(productImageView)
        contentView.addSubview(verticalStackView)
        verticalStackView.addArrangedSubview(nameLabel)
        verticalStackView.addArrangedSubview(detailLabel)
        verticalStackView.addArrangedSubview(priceButton)
    }
    
    func setupConstraints() {
        productImageView.snp.makeConstraints { make in
            make.left.equalTo(contentView).offset(Layout.offset)
            make.centerY.equalTo(contentView)
        }
        verticalStackView.snp.makeConstraints { make in
            make.top.right.bottom.equalTo(contentView).inset(Layout.offset)
            make.left.equalTo(productImageView.snp.right).offset(Layout.offset)
        }
    }
}
