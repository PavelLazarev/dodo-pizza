//
//  SumAndCountCell.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 01.07.2023.
//

import UIKit

final class SumAndCountCell: UITableViewCell {
    
    static let reuseId = "SumAndCountCell"
    
    private let customerStepper = CustomerStepper()
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15)
        
        return label
    }()
    
    private let horizontalStackView: UIStackView = { // вынести в отдельный класс
        let stackView = UIStackView()
        stackView.axis = .horizontal
        
        return stackView
    }()
    
 
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(product: Product) {
        priceLabel.text = String(product.price)
    }
    
}

extension SumAndCountCell {
    
    func setupViews() {
        contentView.addSubview(horizontalStackView)
        horizontalStackView.addArrangedSubview(priceLabel)
        horizontalStackView.addArrangedSubview(customerStepper)
    }
    
    func setupConstraints() {
        horizontalStackView.snp.makeConstraints { make in
            make.top.bottom.left.right.equalTo(contentView).inset(10)
        }
    }
}
