//
//  ProductCartCell.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 04.07.2023.
//

import Foundation
import UIKit


final class ProductCartCell: UITableViewCell {
    
    static let reuseId = "ProductCartCell"
    
    private let detailLabel = Labels(type: .detail)
    private let nameLabel = Labels(type: .name)
    
    private let productImageView: UIImageView = { // поместить в отдельный класс?
        var imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        
        return imageView
    }()
    
    private let verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        
       return stackView
    }()
    
    private let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Method
    func update(product: Product) {
        detailLabel.text = product.description
        nameLabel.text = product.name
        
        if product.image == "1" {
            productImageView.image = UIImage(named: "Домашняя")
        } else if product.image == "2" {
            productImageView.image = UIImage(named: "Цыпленок карри")
        } else if product.image == "3" {
            productImageView.image = UIImage(named: "Маргарита")
        } else if product.image == "4" {
            productImageView.image = UIImage(named: "4 сыра")
        } else {
            productImageView.image = UIImage(named: "Гавайская")
        }
    }
    
}

extension ProductCartCell {
    func setupViews() {
        contentView.addSubview(horizontalStackView)
        contentView.addSubview(productImageView)

        horizontalStackView.addArrangedSubview(verticalStackView)
        
        verticalStackView.addArrangedSubview(nameLabel)
        verticalStackView.addArrangedSubview(detailLabel)
        
    }
    
    func setupConstraints() {
        productImageView.snp.makeConstraints { make in
            make.left.equalTo(contentView).inset(20)
            make.top.bottom.equalTo(contentView).inset(40)
        }
        
        horizontalStackView.snp.makeConstraints { make in
            make.left.equalTo(productImageView.snp.right).offset(10)
            make.right.top.equalTo(contentView).inset(20)
            make.bottom.equalTo(contentView).inset(40)
        }
        
        verticalStackView.snp.makeConstraints { make in
            make.top.equalTo(horizontalStackView).inset(20)
        }
        
    }
}
