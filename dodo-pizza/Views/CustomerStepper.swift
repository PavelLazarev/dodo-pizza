//
//  CustomerStepper.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 03.07.2023.
//

import UIKit
final class CustomerStepper: UIControl {
    
    var currentValue = 1 {
        didSet {
            currentValue = currentValue > 0 ? currentValue : 0
            currentStepValueLable.text = "\(currentValue)"
        }
    }
    
    private lazy var currentStepValueLable: UILabel = {
        let label = UILabel()
        label.text = "\(currentValue)"
        label.font = UIFont.monospacedDigitSystemFont(ofSize: 15, weight: UIFont.Weight.regular)
        
        return label
    }()
    
   private lazy var increaseButton: UIButton = {
       let button = UIButton()
       button.setTitle("+", for: .normal)
       button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
       button.setTitleColor(.black, for: .normal)
       
       return button
   }()
   
    private lazy var  decreaseButton: UIButton = {
       let button = UIButton()
       button.setTitle("-", for: .normal)
       button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
       button.setTitleColor(.black, for: .normal)
       
       return button
   }()
    
    private let horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 15
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setup()
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        self.layer.cornerRadius = 15
        self.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 247/255, alpha: 1)
        
        self.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    private func setupViews() {
        
        addSubview(horizontalStackView)
        horizontalStackView.addArrangedSubview(decreaseButton)
        horizontalStackView.addArrangedSubview(currentStepValueLable)
        horizontalStackView.addArrangedSubview(increaseButton)
    }
    
    private func setupConstraints() {
        horizontalStackView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }
    
    // MARK: - Actions
    
    @objc func buttonTapped(_ sender: UIButton) {
        switch sender {
        case decreaseButton:
            currentValue -= 1
        case increaseButton:
            currentValue += 1
        default:
            break
        }
        sendActions(for: .valueChanged)
    }
}
