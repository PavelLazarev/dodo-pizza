//
//  ConfirmButtonFooterView.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 05.07.2023.
//

import UIKit

final class ConfirmButtonFooterView: UIView {
    
    var onAction: (()->())?
    
    lazy var confirmPriceButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .orange
        button.layer.cornerRadius = 20
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
//        button.setTitle("Подтвердите заказ на сумму 2000р", for: .normal)
        
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // Передача цены в кнопку
    func update(_ price: Int) {
        confirmPriceButton.setTitle("Подтвердить заказа на сумму \(price)", for: .normal)
    }
    
    @objc func confirmButtonTapped(sender: UIButton) {
        print(#function)
        self.onAction?()
    }
}

extension ConfirmButtonFooterView {
    
    func setupViews() {
        addSubview(confirmPriceButton)
    }
    
    func setupConstraints() {
        confirmPriceButton.snp.makeConstraints { make in
            make.left.right.top.bottom.equalTo(self).inset(10)
        }
    }
}
