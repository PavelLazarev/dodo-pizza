//
//  PriceHeaderView.swift
//  dodo-pizza
//
//  Created by Pavel Lazarev Macbook on 05.07.2023.
//

import UIKit

final class PriceHeaderView: UIView {
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.backgroundColor = .white
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
        self.backgroundColor = .white
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(count: Int, price: Int) {
        priceLabel.text = "\(count) шт на сумму \(price) рублей"
    }
}

extension PriceHeaderView {
    
    private func setupViews() {
        addSubview(priceLabel)
    }
    
    private func setupConstraints() {
        priceLabel.snp.makeConstraints { make in
            make.left.right.top.bottom.equalTo(self).inset(20)
        }
    }
}
